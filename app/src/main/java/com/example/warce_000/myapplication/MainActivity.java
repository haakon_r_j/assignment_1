package com.example.warce_000.myapplication;


import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//https://www.simplifiedcoding.net/sqlite-database-in-android-using-sqliteopenhelper-class/

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

  private TextView Cords;
    private Button btnSend;
    private Button btnShow;
    private Button btnDel;

    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSend = (Button) findViewById(R.id.sendCords);
        btnShow = (Button) findViewById(R.id.getCords);
        btnDel = (Button) findViewById(R.id.deleteCords);
        Cords = (TextView) findViewById(R.id.showText);

        db = new DatabaseHelper(this);

        btnSend.setOnClickListener(this);
        btnShow.setOnClickListener(this);
        btnDel.setOnClickListener(this);
    }
    @Override
    public void onClick (View v) {
            if (v == btnSend) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.putExtra("message", "send");
                startActivity(intent);
            }
            else if(v == btnDel){ //If delete button was pressed asks user continue/cancel
                new AlertDialog.Builder(this)
                        .setTitle("Delete data")
                        .setMessage("Are you sure you want to delete all data?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (db.deleteCords()) { //if delete cords returns true, it was not a success
                                    popUpp(String.format(getResources().getString(R.string.error))); // error popUpp
                                }
                                //Pop upp for success!!
                                else popUpp(String.format(getResources().getString(R.string.dbSuccess)));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        //.setIcon(android.R.drawable.ic_dialog_alert) //would set an icon on the dialog
                        .show();
            }
            else showCords();
    }

    private void showCords(){
        Cursor c = db.getCords();
        c.moveToFirst();
        if (c.moveToFirst()) {
            Cords.setText("Latitude: " + c.getString(c.getColumnIndex(DatabaseHelper.COLUMN_CORDS_LAT))
                   + " Longitude: " + c.getString(c.getColumnIndex(DatabaseHelper.COLUMN_CORDS_LONG))
                   + " Altitude:  " + c.getString(c.getColumnIndex(DatabaseHelper.COLUMN_CORDS_ALT)));
        }
        else Cords.setText(R.string.dbEmpty);
        c.close();
}

    public void GoToMapsActivity (){
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void popUpp(String popU) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, popU, duration);
        toast.show();
    }
}

