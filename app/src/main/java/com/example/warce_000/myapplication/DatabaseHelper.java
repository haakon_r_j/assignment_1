package com.example.warce_000.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by warce_000 on 21.09.2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "MapCooordinates";
    public static final String TABLE_NAME = "Cooordinates";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CORDS_LAT="coordinates1";
    public static final String COLUMN_CORDS_LONG ="coordinates2";
    public static final String COLUMN_CORDS_ALT ="co";

    private static final int DB_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " +TABLE_NAME
                +"(" +COLUMN_ID+
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +COLUMN_CORDS_LAT+ " VARCHAR, "
                                                       +COLUMN_CORDS_LONG+ " VARCHAR, "
                                                       +COLUMN_CORDS_ALT+ " VARCHAR);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Cooordinates";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean addCords(String lat, String c_long, String alt){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_CORDS_LAT,lat);
        contentValues.put(COLUMN_CORDS_LONG,c_long);
        contentValues.put(COLUMN_CORDS_ALT,alt);

        db.insert(TABLE_NAME, null, contentValues);
        db.close();
        return true;
    }

    public boolean deleteCords(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "DELETE FROM Cooordinates;";
        db.execSQL(sql);
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        return c.moveToFirst();
    }

    public Cursor getCords(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT coordinates1, coordinates2, co FROM Cooordinates WHERE id = (SELECT MAX(id) FROM Cooordinates);";

        Cursor c = db.rawQuery(sql, null);
        return c;
    }
}
